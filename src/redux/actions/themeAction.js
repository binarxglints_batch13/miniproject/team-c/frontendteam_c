import * as type from './actionTypes';

export const setThemeDarkModeAction = () => ({type: type.SET_THEME_DARK_MODE});
export const unSetThemeDarkModeAction = () => ({type: type.UNSET_THEME_DARK_MODE});
export const toggleThemeDarkModeAction = () => ({type: type.TOGGLE_THEME_DARK_MODE});
