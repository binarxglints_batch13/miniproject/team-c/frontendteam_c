import * as type from '../actions/actionTypes';

const initialState = {
  darkMode: false,
}
const themeReducer = (state = initialState, action) => {
  switch(action.type){
    case type.SET_THEME_DARK_MODE: return {
      ...state,
      darkMode: true
    };
    case type.UNSET_THEME_DARK_MODE: return {
      ...state,
      darkMode: false
    };
    case type.TOGGLE_THEME_DARK_MODE: return {
      ...state,
      darkMode: !state.darkMode
    }
    default: return state;
  }
}

export default themeReducer
