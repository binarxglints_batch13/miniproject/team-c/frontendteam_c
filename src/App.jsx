import React, { useEffect } from 'react';
import { ThemeProvider, createTheme } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import { connect } from 'react-redux';

import {
  useDispatch
  // , useSelector 
} from 'react-redux'
import { getMovieCategoryAction, getMoviesAction, getTopRatingMoviesAction } from './redux/actions/moviesAction'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from './components/Header';
import Footer from './components/Footer';
import HomePage from './pages/HomePage';
import DetailsPage from './pages/DetailsPage';
import UserProfilePage from './pages/UserProfilePage';
import AdminPage from './pages/AdminPage';
// import BackdropSpinner from './components/loading/BackdropSpinner';
import { getLogedInUser } from './redux/actions/userAction';







const App = ({theme}) => {
  const dispatch = useDispatch()
  // const {loading} = useSelector(state => state.movies);
  // console.log(loading);

  const rootTheme = createTheme({
    palette: {
      type: theme.darkMode ? 'dark' : 'light',
      primary: {
        light: "#FA8844",
        main: "#BB6533",
        dark: "#7D4322"
      },
      secondary: {
        light: "#EB507F",
        main: "#FE024E",
        dark: "#7C2326"
      }
    }
  })

  useEffect(() => {
    dispatch(getMoviesAction({ page: 1, size: 10 }))
    dispatch(getTopRatingMoviesAction(1));
    dispatch(getMovieCategoryAction());
    const token = localStorage.getItem('token');
    if (token) {
      dispatch(getLogedInUser());
    }
  }, [dispatch])
  return (
    <ThemeProvider theme={rootTheme}>
      <CssBaseline />
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={() => (<HomePage />)} /> {/**HomePage */}
          <Route path="/details/:movie_id" component={() => (<DetailsPage />)} />
          <Route path="/user/profile" component={() => (<UserProfilePage />)} />
          <Route path="/admin" component={() => (<AdminPage />)} />
        </Switch>
        <Footer />
      </Router>
    </ThemeProvider>
  )
}

const mapStateToProps = (state) => ({
  theme: state.theme
})
export default connect(mapStateToProps)(App)
